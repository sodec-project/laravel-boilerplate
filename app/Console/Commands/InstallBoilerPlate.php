<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InstallBoilerPlate extends Command
{
    public $envfile;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installation de l\'application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->envFile = file_get_contents(__DIR__.'/../../../.env');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Packages
        $p_diagnosis = $this->ask("Voulez-vous Installer le packages: laravel-self-diagnosis ? [Y/N]");
        $p_image = $this->ask("Voulez-vous Installer le packages: intervention/image ? [Y/N]");
        $p_horizon = $this->ask("Voulez-vous Installer le packages: horizon ? [Y/N]");
        $p_pdf = $this->ask("Voulez-vous Installer le packages: laravel-dompdf ? [Y/N]");
        $p_larecipe = $this->ask("Voulez-vous Installer le packages: larecipe ? [Y/N]");
        $this->comment("Installation des packages obligatoire...");
        $this->installSlackNotificationChannel();
        $this->installPredis();
        $this->installTelescope();
        $this->installLogViewer();
        $this->comment("Installation des packages obligatoire: TERMINER");
        $this->comment("Installation des packages facultatifs...");
        if($p_diagnosis == 'Y' || $p_diagnosis == 'y'){
            $this->installSelfDiagnosis();
        }
        if($p_image == 'Y' || $p_image == 'y'){
            $this->installInterventionImage();
        }
        if($p_horizon == 'Y' || $p_horizon == 'y'){
            $this->installHorizon();
        }
        if($p_pdf == 'Y' || $p_pdf == 'y'){
            $this->installLaravelDomPdf();
        }
        if($p_larecipe == 'Y' || $p_larecipe == 'y'){
            $this->installLarecipe();
        }
        $this->comment("Installation des packages facultatifs: TERMINER");
        $this->comment("Migration...");
        $this->call('migrate');
        $this->comment("Migration: TERMINER");
        $this->info("Installation de l'application terminer");
    }

    private function installSlackNotificationChannel()
    {
        $this->info("Installation de slack-notification-channel...");
        exec("composer require laravel/slack-notification-channel --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->info("Installation de slack-notification-channel: TERMINER");
    }

    private function installPredis()
    {
        $this->info("Installation de predis...");
        exec("composer require predis/predis --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->info("Installation de predis: TERMINER");
    }

    private function installTelescope()
    {
        $this->info("Installation de laravel telescope...");
        exec("composer require laravel/telescope --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->call('vendor:publish', [
            "--provider" => "Laravel\Telescope\TelescopeServiceProvider"
        ]);
        $this->call('telescope:install');
        if(env('APP_ENV') == 'local' || env('APP_ENV') == 'testing') {
            str_replace('TELESCOPE_ENABLED=false', 'TELESCOPE_ENABLED=true', $this->envfile);
        }
        $this->info("Installation de laravel telescope: TERMINER");
    }

    private function installLogViewer()
    {
        $this->info("Installation de log-Viewer...");
        exec("composer require arcanedev/log-viewer:~7.0 --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->call("vendor:publish", [
            "--provider" => "Arcanedev\LogViewer\LogViewerServiceProvider"
        ]);
        $this->call('log-viewer:publish');
        $this->info("Installation de log-Viewer: TERMINER");
    }

    private function installSelfDiagnosis()
    {
        $this->info("Installation de laravel-self-diagnosis...");
        exec("composer require beyondcode/laravel-self-diagnosis --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->info("Installation de laravel-self-diagnosis: TERMINER");
    }

    private function installInterventionImage()
    {
        $this->info("Installation de intervention/images...");
        exec("composer require intervention/image --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->call('vendor:publish', [
            '--provider' => 'Intervention\Image\ImageServiceProviderLaravelRecent'
        ]);
        $this->info("Installation de intervention/images: TERMINER");
    }

    private function installHorizon()
    {
        $this->info("Installation de laravel Horizon...");
        exec("composer require laravel/horizon --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->call('vendor:publish', [
            "--provider" => "Laravel\Horizon\HorizonServiceProvider"
        ]);
        $this->call('horizon:install');
        $this->info("Installation de laravel Horizon: TERMINER");
    }

    private function installLaravelDomPdf()
    {
        $this->info("Installation de laravel DOMPDF...");
        exec("composer require barryvdh/laravel-dompdf --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->call('vendor:publish', [
            '--provider' => 'Barryvdh\DomPDF\ServiceProvider'
        ]);
        $this->info("Installation de laravel DOMPDF: TERMINER");
    }

    private function installLarecipe()
    {
        $this->info("Installation de LaRecipe...");
        exec("composer require binarytorch/larecipe --ignore-platform-reqs");
        exec("composer dumpautoload");
        $this->call("vendor:publish", [
            "--provider" => "BinaryTorch\LaRecipe\LaRecipeServiceProvider"
        ]);
        $this->call('larecipe:install');
        $this->info("Installation de LaRecipe: TERMINER");
    }
}
